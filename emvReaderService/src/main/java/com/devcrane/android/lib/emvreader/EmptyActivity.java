
package com.devcrane.android.lib.emvreader;

import android.app.Activity;
import android.os.Bundle;

public class EmptyActivity extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		finish();
	}
}
