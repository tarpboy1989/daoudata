/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.devcrane.android.lib.emvreader;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.devcrane.android.lib.emvreader";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
