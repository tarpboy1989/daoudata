package com.devcrane.payfun.daou.dialog;

import com.devcrane.payfun.daou.entity.ReceiptEntity;

/**
 * Created by admin on 6/2/17.
 */

public interface CancelListDialogListener {
    public void CancelListDialogEvent(ReceiptEntity entity);
}
