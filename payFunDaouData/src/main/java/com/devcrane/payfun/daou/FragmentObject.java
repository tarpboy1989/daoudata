package com.devcrane.payfun.daou;

/**
 * Created by admin on 7/28/17.
 */

public class FragmentObject {
    public Class<?> mClass;
    public int titleId;

    public FragmentObject(Class<?> pClass, int titleId) {
        this.mClass = pClass;
        this.titleId = titleId;
    }
}