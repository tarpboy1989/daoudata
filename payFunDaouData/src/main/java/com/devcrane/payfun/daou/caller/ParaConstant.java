package com.devcrane.payfun.daou.caller;

public class ParaConstant {
	public static final String PAYMENT_TYPE_CREDIT="credit";
	public static final String PAYMENT_TYPE_CASH="cash";
	public static final String TRANS_TYPE_APPROVE="01";
	public static final String TRANS_TYPE_CANCEL="02";
	public static final String TRANS_RESULT_SUCCESS="00";
	public static final String TRANS_RESULT_FAIL="99";
	public static final String VALUE_EMPTY="";

}
