package com.devcrane.payfun.daou.van;

public class DaouDataContants {
	public static final String CREDIT_REQ ="0200";
	public static final String CREDIT_RES ="0210";
	public static final String CREDIT_EMV_REQ ="0100";
	public static final String CREDIT_EMV_RES ="0110";
	public static final String CREDIT_CANCEL_REQ ="0420";
	public static final String CREDIT_CANCEL_RES ="0430";
	public static final String CREDIT_EMV_CANCEL_REQ ="0400";
	public static final String CREDIT_EMV_CANCEL_RES ="0410";
	public static final String TASK_CREDIT ="10";
	
	public static final String REGISTERED_TELEPHONE_REQ ="0200";
	public static final String REGISTERED_TELEPHONE_RES ="0210";
	public static final String TASK_REGISTERED ="12";
	
	public static final String DEVICE_SETTLEMENT_REQ="0200";
	public static final String DEVICE_SETTLEMENT_RES="0210";
	public static final String TASK_DEVICE_SETTLEMENT ="13";
	
	public static final String BL_VERIFICATION_REQ="0200";
	public static final String BL_VERIFICATION_RES="0210";
	public static final String TASK_BL_VERIFICATION ="16";

	public static final String EMV_PUBLIC_KEY_DETAIL_REQ="0100";
	public static final String EMV_PUBLIC_KEY_DETAIL_RES="0110";
	public static final String TASK_EMV_PUBLIC_KEY_DETAIL ="76";
	
	public static final String EMV_PUBLIC_KEY_DOWNLOAD_REQ="0100";
	public static final String EMV_PUBLIC_KEY_DOWNLOAD_RES="0110";
	public static final String TASK_EMV_PUBLIC_KEY_DOWNLOAD ="77";
	
	public static final String SEND_TC_ISSUER_SCRIPT_RESULT_REQ="0100";
	public static final String SEND_TC_ISSUER_SCRIPT_RESULT_RES="0110";
	public static final String TASK_SEND_TC_ISSUER_SCRIPT_RESULT ="78";
	
	
	public static final String UNION_PAY_CARD_REQ ="0200";
	public static final String UNION_PAY_CARD_RES ="0210";
	
	public static final String UNION_PAY_CARD_EMV_REQ ="0100";
	public static final String UNION_PAY_CARD_EMV_RES ="0110";
	
	public static final String UNION_PAY_CARD_CANCEL_REQ ="0420";
	public static final String UNION_PAY_CARD_CANCEL_RES ="0430";
	
	public static final String UNION_PAY_CARD_EMV_CANCEL_REQ ="0400";
	public static final String UNION_PAY_CARD_EMV_CANCEL_RES ="0410";
	
	public static final String TASK_UNION_PAY_CARD ="17";
	
	
	public static final String INQUIRY_OF_CHEQUE_REQ="0200";
	public static final String INQUIRY_OF_CHEQUE_RES="0210";
	public static final String TASK_INQUIRY_OF_CHEQUE ="15";
	
	public static final String INQUIRY_OF_EXCHANGE_RATE_REQ="0200";
	public static final String INQUIRY_OF_EXCHANGE_RATE_RES="0210";
	
	public static final String INQUIRY_OF_EXCHANGE_RATE_EMV_REQ="0100";
	public static final String INQUIRY_OF_EXCHANGE_RATE_EMV_RES="0110";
	
	public static final String TASK_INQUIRY_OF_EXCHANGE_RATE ="1C";
	
	
	public static final String LOCAL_CURRENCY_APPROVAL_REQ="0200";
	public static final String LOCAL_CURRENCY_APPROVAL_RES="0210";
	
	public static final String LOCAL_CURRENCY_APPROVAL_EMV_REQ="0100";
	public static final String LOCAL_CURRENCY_APPROVAL_EMV_RES="0110";
	
	public static final String TASK_LOCAL_CURRENCY_APPROVAL ="1D";
	
	
	public static final String TAX_REFUND_CREDIT_REQ="0200";
	public static final String TAX_REFUND_CREDIT_RES="0210";
	public static final String TAX_REFUND_CREDIT_EMV_REQ="0100";
	public static final String TAX_REFUND_CREDIT_EMV_RES="0110";
	public static final String TAX_REFUND_CREDIT_CANCEL_REQ="0420";
	public static final String TAX_REFUND_CREDIT_CANCEL_RES="0430";
	public static final String TAX_REFUND_CREDIT_EMV_CANCEL_REQ="0400";
	public static final String TAX_REFUND_CREDIT_EMV_CANCEL_RES="0410";
	public static final String TASK_TAX_REFUND_CREDIT ="10";
	
	public static final String TAX_REFUND_CASH_REQ="0200";
	public static final String TAX_REFUND_CASH_RES="0210";
	public static final String TAX_REFUND_CASH_CANCEL_REQ="0420";
	public static final String TAX_REFUND_CASH_CANCEL_RES="0430";
	public static final String TASK_TAX_REFUND_CASH ="7A";
	
	public static final String INQUIRY_OF_TAX_REFUND_REQ="0200";
	public static final String INQUIRY_OF_TAX_REFUND_RES="0210";
	public static final String TASK_INQUIRY_OF_TAX_REFUND ="7B";
	
	public static final String TAX_REFUND_OPENING_REQ="0200";
	public static final String TAX_REFUND_OPENING_RES="0210";
	public static final String TASK_TAX_REFUND_OPENING ="7C";
	
	public static final String TAX_REFUND_ITEM_LIST_DOWNLOAD_REQ="0200";
	public static final String TAX_REFUND_ITEM_LIST_DOWNLOAD_RES="0210";
	public static final String TASK_TAX_REFUND_ITEM_LIST_DOWNLOAD ="7D";
	
	public static final String INQUIRY_IF_POINT_REQ="0200";
	public static final String INQUIRY_IF_POINT_RES="0210";
	public static final String TASK_INQUIRY_IF_POINT ="30";
	
	public static final String SAVING_POINT_REQ="0200";
	public static final String SAVING_POINT_RES="0210";
	public static final String SAVING_POINT_CANCEL_REQ="0420";
	public static final String SAVING_POINT_CANCEL_RES="0430";
	
	public static final String TASK_SAVING_POINT ="31";
	
	
	public static final String USING_POINT_REQ="0200";
	public static final String USING_POINT_RES="0210";
	public static final String UING_POINT_CANCEL_REQ="0420";
	public static final String USING_POINT_CANCEL_RES="0430";
	
	public static final String TASK_USING_POINT ="32";
	
	
	public static final String POINT_DISCOUNT_REQ="0200";
	public static final String POINT_DISCOUNT_RES="0210";
	public static final String POINT_DISCOUNT_CANCEL_REQ="0420";
	public static final String POINT_DISCOUNT_CANCEL_RES="0430";
	
	public static final String TASK_POINT_DISCOUNT ="35";
	
	
	public static final String MEMBERSHIP_INQUIRY_REQ="0200";
	public static final String MEMBERSHIP_INQUIRY_RES="0210";
	public static final String TASK_MEMBERSHIP_INQUIRY ="33";
	
	
	public static final String MEMBERSHIP_REQ="0200";
	public static final String MEMBERSHIP_RES="0210";
	public static final String MEMBERSHIP_CANCEL_REQ="0420";
	public static final String MEMBERSHIP_CANCEL_RES="0430";
	
	public static final String TASK_MEMBERSHIP ="34";
	
	
	public static final String CASH_RECEIPT_REQ="0200";
	public static final String CASH_RECEIPT_RES="0210";
	public static final String CASH_RECEIPT_CANCEL_REQ="0420";
	public static final String CASH_RECEIPT_CANCEL_RES="0430";
	
	public static final String TASK_CASH_RECEIPT ="40";
	public static final String TASK_NO_EOT ="99";
	
	
	public static final String SALES_REGISTRATION_SAVING_REQ="0200";
	public static final String SALES_REGISTRATION_SAVING_RES="0210";
	public static final String SALES_REGISTRATION_SAVING_CANCEL_REQ="0420";
	public static final String SALES_REGISTRATION_SAVING_CANCEL_RES="0430";
	
	public static final String TASK_SALES_REGISTRATION_SAVING ="41";
	
	
	public static final String PURCHASE_REQ="0200";
	public static final String PURCHASE_RES="0210";
	public static final String REFUND_PURCHASING_REQ="0400";
	public static final String REFUND_PURCHASING_RES="0410";
	
	public static final String TASK_PURCHASING ="50";
	
	
	public static final String BALANCE_INQUIRY_REQ="0100";
	public static final String BALANCE_INQUIRY_RES="0110";
	public static final String TASK_BALANCE_INQUIRY ="54";
	
	
	public static final String RESULT_OF_INQUIRY_REQ="0100";
	public static final String RESULT_OF_INQUIRY_RES="0110";
	public static final String TASK_RESULT_OF_INQUIRY ="55";

	public static final String RAMDOM_NUMBER_GENERATION_REQ="0100";
	public static final String RAMDOM_NUMBER_GENERATION_RES="0110";
	public static final String TASK_RAMDOM_NUMBER_GENERATION ="5B";
	
	public static final String DEVICE_OPENING_TRANSACTION_REQ="0200";
	public static final String DEVICE_OPENING_TRANSACTION_RES="0210";
	public static final String TASK_DEVICE_OPENING_TRANSACTION ="90";
	
	
	public static final String RENEWAL_OF_DEVICE_ENCRYPT_KEY_REQ="0200";
	public static final String RENEWAL_OF_DEVICE_ENCRYPT_KEY_RES="0210";
	public static final String TASK_RENEWAL_OF_DEVICE_ENCRYPT_KEY ="91";
	
	public static final String RECEIVED_SLIP_MESSAGE_REQ="0200";
	public static final String RECEIVED_SLIP_MESSAGE_RES="0210";
	public static final String TASK_RECEIVED_SLIP_MESSAGE ="95";

	
	public static final String COLLECTED_OIL_PRICE_REQ="0200";
	public static final String COLLECTED_OIL_PRICE_RES="0210";
	public static final String TASK_COLLECTED_OIL_PRICE ="9B";
	
	public static final String TERMINAL_SECURITY_CERTIFICATION_REQ="0200";
	public static final String TERMINAL_SECURITY_CERTIFICATION_RES="0210";
	public static final String TASK_TERMINAL_SECURITY_CERTIFICATION ="9C";
	
	public static final String TERMINAL_SECURITY_KEY_DOWNLOAD_REQ="0200";
	public static final String TERMINAL_SECURITY_KEY_DOWNLOAD_RES="0210";
	public static final String TASK_TERMINAL_SECURITY_KEY_DOWNLOAD ="9D";

	public static final String INCOMPLETE_TRANSACTION_REQ="0420";
	public static final String INCOMPLETE_TRANSACTION_RES="0430";
	public static final String TASK_INCOMPLETE_TRANSACTION ="99";


	public static final byte VAL_STX =0x02;
	public static final byte VAL_ETX =0x03;
	public static final byte VAL_EOT =0x04;
	public static final byte VAL_ENQ =0x05;
	public static final byte VAL_ACK =0x06;
	public static final byte VAL_NAK =0x15;
	public static final byte VAL_DLE =0x10;
	public static final byte VAL_FS =0x1c;

	public static final String VAL_WCC_KEYIN ="K";
	public static final String VAL_WCC_SWIPE ="S";
	public static final String VAL_WCC_IC ="I";
	public static final String VAL_VERSION_DIVISION_V2 ="V2";
	public static final String VAL_TERMINAL_NUMBER ="55000612";
	public static final String VAL_MODULE_ID ="1000000011";

	public static final String VAL_PRODUCTION_SERIAL_NUMBER =""; //356110899999
	public static final String VAL_COMPANY_NUMBER ="";
	public static final String VAL_HW_CERT_DEFAULT =""; //DAOUPAYFUNV11000
	public static final String VAL_SW_CERT_DEFAULT ="DAP12PAYP0001000";



	
	public static final String VAL_TERMINAL_DIVISION_GENERAL ="0";
	public static final String VAL_TERMINAL_DIVISION_GAS ="1";
	public static final String VAL_TERMINAL_DIVISION_CHARGING ="2";
	public static final String VAL_TERMINAL_DIVISION_DUTY_FREE_OIL ="3";
	public static final String VAL_TERMINAL_DIVISION_TAXABLE_TAXFREE ="4";
	public static final String VAL_TERMINAL_DIVISION_PAYBACK ="P";
	public static final String VAL_TERMINAL_DIVISION_KING_POINT_TRANS ="W";
	public static final String VAL_TERMINAL_DIVISION_LOCAL_CURR_PAY ="D";
	public static final String VAL_TERMINAL_DIVISION_TAX_REFUND ="T";
	public static final String VAL_TERMINAL_DIVISION_CASH_IC ="K";
	public static final String VAL_TERMINAL_DIVISION_OFFLINE_PC ="G";
	public static final String VAL_TERMINAL_DIVISION_MULTI_VENDOR ="M";
	
	public static final String VAL_DIVISION_DONGLE ="W";
	
	public static final String VAL_ELECTRONIC_SIGN ="S";
	public static final String VAL_ELECTRONIC_OTHERS ="A";
	
	public static final String VAL_RF_CARD_VISA_WAVE ="W";
	
	public static final String VAL_NO_SLIP_DIVISION ="  ";
	
	public static final String VAL_PC_POS ="Y";
	public static final String VAL_PC_POS_OTHER =" ";
	
	public static final String VAL_KEY_DIVISION_DUKPT ="D";
	public static final String VAL_KEY_DIVISION_NIPP ="N";
	
	public static final String VAL_MODEL_CODE ="PK0N";

	public static final String VAL_CARD_TYPE_DEFAULT="J";


	public static final String VAL_RESP_CODE_SUCCESS="0000";

	public static String SWModelName="DAP12PAYP000";//static
	public static String SWModelNo = "1000";//static

//	public static String HWModelName = "DAOUPAYFUND0";//from device
//	public static String HWModelNo = "1000";//from device


	public static final String VAL_FALL_BACK_REASON_NO_RESPONSE="01";
	public static final String VAL_FALL_BACK_REASON_NO_SUPPORT_APPICATION="02";
	public static final String VAL_FALL_BACK_REASON_NOT_ICC="03";
	public static final String VAL_FALL_BACK_REASON_UNDER_COVERAGE_MANDATORY_DATA="04";
	public static final String VAL_FALL_BACK_REASON_FAIL_CVM_CMD_RESPONSE="05";
	public static final String VAL_FALL_BACK_REASON_SET_EMV_CMD_INCORRECT="06";
	public static final String VAL_FALL_BACK_REASON_TERMINAL_MAL_FUNCTION="07";
	public static final String VAL_INCOMPLETE_REASON_WRITE_BACK_DECLINE="2";
	public static final String VAL_INCOMPLETE_REASON_WRITE_BACK_IC_CARD_REMOVED="3";
	public static final String VAL_INCOMPLETE_REASON_WRITE_BACK_OTHERS="4";

}
