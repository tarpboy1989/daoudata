package com.devcrane.payfun.daou.dialog;

/**
 * Created by admin on 7/19/17.
 */

public interface VanDLStep2DialogListener {
    public void VanDLStep2DialogEvent(String companyNO);
    public void VanDLStep2DialogEvent(boolean isValid);
}
