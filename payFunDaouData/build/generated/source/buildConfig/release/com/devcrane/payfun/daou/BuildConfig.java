/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.devcrane.payfun.daou;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.devcrane.payfun.daou";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 135;
  public static final String VERSION_NAME = "1.0.0.1";
}
